require 'rails_helper'

describe InvoicesController do
  before do
    request.headers.merge! 'API_KEY' => api_key
  end

  describe 'GET show' do
    let(:integration) { create(:integration) }
    let(:invoice)     { create(:invoice, practice: integration.practice) }
    let(:api_key)     { integration.partner.api_key }

    let(:integrated_invoice) do
      create(:integrated_invoice, integration: integration, invoice: invoice)
    end

    subject do
      get :show, format: :json, params: {
        id: integrated_invoice.uid, practice_id: invoice.practice.id
      }
    end

    it { is_expected.to be_success }

    context 'when the partner is not integrated' do
      let(:api_key) { create(:partner, api_key: 'other_api_key').api_key }

      it { is_expected.to be_not_found }
    end

    context 'when the partner has no integration with the invoice' do
      let(:other_partner) { create(:partner, api_key: 'other_api_key') }
      let(:api_key)       { other_partner.api_key }

      let!(:other_integration) do
        create(
          :integration,
          partner: other_partner, practice: integration.practice
        )
      end

      it { is_expected.to be_not_found }
    end
  end
end
