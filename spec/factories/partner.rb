FactoryGirl.define do
  factory :partner do
    name      'Some Partner'
    api_key   'some_api_key'
  end
end
