FactoryGirl.define do
  factory :practice do
    name        'Some Practice'
    identifier  'some_practice'
  end
end
