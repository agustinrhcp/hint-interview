FactoryGirl.define do
  factory :integration do
    practice
    partner
    active    true
  end
end
