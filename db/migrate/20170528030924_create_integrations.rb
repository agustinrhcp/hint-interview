class CreateIntegrations < ActiveRecord::Migration[5.1]
  def change
    create_table :integrations do |t|
      t.boolean :active
      t.references :practice
      t.references :partner
    end
  end
end
