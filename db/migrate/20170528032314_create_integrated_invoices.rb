class CreateIntegratedInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :integrated_invoices do |t|
      t.references  :integration
      t.references  :invoice
      t.string      :uid
    end
  end
end
