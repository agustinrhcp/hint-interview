# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170528032314) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "integrated_invoices", force: :cascade do |t|
    t.bigint "integration_id"
    t.bigint "invoice_id"
    t.string "uid"
    t.index ["integration_id"], name: "index_integrated_invoices_on_integration_id"
    t.index ["invoice_id"], name: "index_integrated_invoices_on_invoice_id"
  end

  create_table "integrations", force: :cascade do |t|
    t.boolean "active"
    t.bigint "practice_id"
    t.bigint "partner_id"
    t.index ["partner_id"], name: "index_integrations_on_partner_id"
    t.index ["practice_id"], name: "index_integrations_on_practice_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.bigint "practice_id"
    t.index ["practice_id"], name: "index_invoices_on_practice_id"
  end

  create_table "partners", force: :cascade do |t|
    t.string "name"
    t.string "api_key"
  end

  create_table "practices", force: :cascade do |t|
    t.string "name"
    t.string "identifier"
  end

end
