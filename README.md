## Api Docs

#### Show Invoice
##### URL
`/practices/:practice_uid/invoices/:invoice_uid`

##### Method
`GET`

#### URL params
- `practice_uid`
- `invoice_uid`

#### Success Response
`Code: 200`

`Content:`
```
{
  uid: '...',
  invoice_uid: '...',
  description: '...',
  ...
}
```

#### Error Responses
`Code: 401`
`Content: { error: 'Unauthorized' }`

`Code: 404`
`Content: { error: 'Practice not found' }`

`Code: 404`
`Content: { error: 'Invoice not found' }`
***

#### Fetch Invoices
##### URL
`/practices/:practice_uid/invoices`

##### Method
`GET`

#### URL params
- `practice_uid`

#### Success Response
`Code: 200`

`Content:`
```
[
  {
    uid: '...',
    practice_uid: '...',
    description: '...',
    ...
  },
  {
    ...
  }
]
```

#### Error Responses
`Code: 401`
`Content: { error: 'Unauthorized' }`

`Code: 404`
`Content: { error: 'Practice not found' }`
***

#### Create Invoice

##### URL
`/practices/:practice_uid/invoices`

##### Method
`POST`

#### URL params
- `practice_uid`

#### Success Response
`Code: 201`

`Content:`
```
{
  uid: '...',
  invoice_uid: '...',
  description: '...',
  ...
}
```

#### Error Responses

`Code: 422`
`Content: { error: 'Invoice already exist' }`

`Code: 401`
`Content: { error: 'Unauthorized' }`

`Code: 404`
`Content: { error: 'Practice not found' }`

 ***

#### Update Invoice

##### URL
`/practices/:practice_uid/invoices/:invoice_uid`

##### Method
`PUT`

#### URL params
- `practice_uid`
- `invoice_uid`

#### Success Response
`Code: 200`

`Content:`
```
{
  uid: '...',
  invoice_uid: '...',
  description: '...',
  ...
}
```

#### Error Responses

`Code: 401`
`Content: { error: 'Unauthorized' }`

`Code: 404`
`Content: { error: 'Practice not found' }`

`Code: 404`
`Content: { error: 'Invoice not found' }`

 ***

#### Destroy Invoice

##### URL
`/practices/:practice_uid/invoices/:invoice_uid`

##### Method
`DELETE`

#### URL params
- `practice_uid`
- `invoice_uid`

#### Success Response
`Code: 200`

`Content:`
```
{
  uid: '...',
  invoice_uid: '...',
  description: '...',
  ...
}
```

#### Error Responses

`Code: 401`
`Content: { error: 'Unauthorized' }`

`Code: 404`
`Content: { error: 'Practice not found' }`

`Code: 404`
`Content: { error: 'Invoice not found' }`

 ***

_Notes_

- _The endpoints for Patients, Practitioners, etc should be similar._

- _404 Error responses can also indicate that the partner has no permission over the practice (there is no integration between them). A 401 error may also be valid._

- _I assumed that deleting a record uid is the same as deleting the association between the Integration and (in this case) an invoice._
