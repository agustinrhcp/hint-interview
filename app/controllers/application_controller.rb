class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from 'ActiveRecord::RecordNotFound' do |error|
    render json: { error: error.message }, status: 404
  end
end
