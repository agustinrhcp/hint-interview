class InvoicesController < ApplicationController
  def show
    @integrated_invoice = integration.integrated_invoices.find_by!(uid: params[:id])
  end

  private

  def integration
    @integration ||= Integration.find_by!(
      partner_id: current_partner.id,
      practice_id: params[:practice_id]
    )
  end

  def current_partner
    @current_partner ||= Partner.find_by!(api_key: request.headers['API_KEY'])
  end
end
