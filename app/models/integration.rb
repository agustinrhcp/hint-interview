class Integration < ActiveRecord::Base
  belongs_to :partner
  belongs_to :practice
  has_many :integrated_invoices
  has_many :invoices, through: :integrated_invoices
end
