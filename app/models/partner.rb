class Partner < ActiveRecord::Base
  has_many :integrations
  has_many :practices, through: :integrations
end
