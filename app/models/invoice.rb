class Invoice < ActiveRecord::Base
  belongs_to :practice
  has_many :integrated_invoices
  has_many :integrations, through: :integrated_invoices
end
