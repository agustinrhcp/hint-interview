class IntegratedInvoice < ActiveRecord::Base
  belongs_to :integration
  belongs_to :invoice
end
