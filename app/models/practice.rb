class Practice < ActiveRecord::Base
  has_many :integrations
  has_many :partners, through: :integrations
  has_many :invoices
end
