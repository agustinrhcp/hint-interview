Rails.application.routes.draw do
  resources :practices, only: [] do
    resources :invoices
  end
end
